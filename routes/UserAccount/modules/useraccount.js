// ------------------------------------
// Constants
// ------------------------------------
import axios from 'axios'
export let origin = window.location.hostname
if (origin === 'localhost' || origin === 'dev.itti.com.pl') {
  origin = 'dev.itti.com.pl:8003'
}
import { errorHandle, getHeaders } from '../../../store/addons'
import { getToastAction } from '../../../store/toasts'
export const GET_USERS = 'GET_USERS'
export const EDIT_USERS = 'EDIT_USERS'
export const RESET_PASSWORD = 'RESET_PASSWORD'

// ------------------------------------
// Actions
// ------------------------------------

export const getUsersAction = (data = []) => {
  return {
    type: GET_USERS,
    data: data
  }
}

export const editUsersAction = (data = null) => {
  return {
    type: EDIT_USERS,
    data: data
  }
}

export const resetPasswordAction = () => {
  return {
    type: RESET_PASSWORD
  }
}

export const actions = {
  getUsers,
  editUsers,
  resetPassword
}

export const getUsers = (id) => {
  return (dispatch) => {
    return new Promise((resolve) => {
      axios.get(`http://${origin}/api/users/current`, getHeaders())
         .then((response) => {
           dispatch(getUsersAction(response.data))
           resolve()
         })
         .catch((error) => {
           errorHandle(error.response.status)
           resolve()
         })
    })
  }
}

export const editUsers = (users) => {
  return (dispatch) => {
    return new Promise((resolve) => {
      axios.put(`http://${origin}/api/users/current`, users, getHeaders())
       .then((response) => {
         dispatch(editUsersAction(users))
         dispatch(getToastAction('User has been edited correctly!', true))
         resolve()
       })
       .catch((error) => {
         if (error.response.status === 400) {
           dispatch(getToastAction('Something get wrong, check your email data'), false)
         }
         resolve()
       })
    })
  }
}

export const resetPassword = (password) => {
  return (dispatch) => {
    return new Promise((resolve) => {
      axios.put(`http://${origin}/api/users/current-change-password`, password, getHeaders())
       .then((response) => {
         dispatch(getToastAction('Password has been changed', true))
         resolve()
       })
       .catch((error) => {
         if (error.response.status === 403) {
           dispatch(getToastAction('Something get wrong, check your current password'), false)
         }
         resolve()
       })
    })
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GET_USERS]: (state, action) => {
    return {
      ...state,
      users: action.data,
      shouldUpdate: false
    }
  },
  [EDIT_USERS]: (state, action) => {
    return {
      ...state,
      shouldUpdate: true,
      users: action.data
    }
  },
  [RESET_PASSWORD]: (state, action) => {
    return {
      ...state,
      shouldUpdate: true
    }
  }
}
// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  users: {},
  password: {},
  shouldUpdate: false
}

export default function homecontentReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
