import { connect } from 'react-redux'
import UserAccount from '../components/UserAccount'
import { getUsers, editUsers, resetPassword } from '../modules/useraccount'

const mapDispatchToProps = {
  getUsers,
  editUsers,
  resetPassword
}

const mapStateToProps = (state) => ({
  isLoggedIn: state.login.isLoggedIn,
  userAccount: state.useraccount.users
})

export default connect(mapStateToProps, mapDispatchToProps)(UserAccount)
