import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path : 'useraccount',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const UserAccountContent = require('./containers/UserAccount').default
      const reducer = require('./modules/useraccount').default
      injectReducer(store, { key: 'useraccount', reducer })
      cb(null, UserAccountContent)
    }, 'useraccount')
  }
})
