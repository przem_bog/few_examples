import React, { Component } from 'react'
import './UserAccount.scss'
import UserAccountContent from '../../../components/UserAccountContent'
import PropTypes from 'prop-types'

class UserAccount extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  static propTypes = {
    getUsers: PropTypes.func,
    userAccount: PropTypes.any,
    editUsers: PropTypes.func,
    resetPassword: PropTypes.func
  }

  render () {
    return (
      <div>
        <UserAccountContent
          getUsers={this.props.getUsers}
          userAccount={this.props.userAccount}
          editUsers={this.props.editUsers}
          resetPassword={this.props.resetPassword}
        />
      </div>
    )
  }
}

export default UserAccount
