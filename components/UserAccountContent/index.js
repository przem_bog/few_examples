import { connect } from 'react-redux'
import UserAccountContent from './component/UserAccountContent'

const mapDispatchToProps = {}

const mapStateToProps = (state) => ({
  isLoggedIn: state.login.isLoggedIn
})

export default connect(mapStateToProps, mapDispatchToProps)(UserAccountContent)
