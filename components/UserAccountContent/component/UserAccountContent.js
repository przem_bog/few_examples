import React, { Component } from 'react'
import './UserAccount.scss'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import { browserHistory } from 'react-router'
import Divider from 'material-ui/Divider'
import { orange500 } from 'material-ui/styles/colors'
import TextField from 'material-ui/TextField'
import ResetDialog from '../../../components/ResetDialog/ResetDialog'

const styles = {
  divider: {
    backgroundColor: orange500
  },
  button: {
    height: 45
  }
}

class UserAccountContent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      openEditInformation: false,
      openPasswordInormation: false,
      users: {
        userId: '',
        firstName: '',
        lastName: '',
        email: '',
        contact: '',
        city: '',
        activated: true,
        rolesIds: [1],
        positionId: 1,
        editFirstName: '',
        editLastName: '',
        editEmail: '',
        editContact: '',
        editCity: '',
        emailErrorText: ''
      },
      firstNameErrorText: '',
      lastNameErrorText: '',
      contactErrorText: '',
      cityErrorText: '',
      shouldUpdate: false
    }
    props.getUsers()
  }

  static propTypes = {
    isLoggedIn: PropTypes.bool,
    getUsers: PropTypes.func,
    userAccount: PropTypes.any,
    editUsers: PropTypes.func,
    shouldUpdate: PropTypes.bool,
    resetPassword: PropTypes.func
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.userAccount) {
      let change = { ...this.state.users }
      change.firstName = nextProps.userAccount.firstName
      change.lastName = nextProps.userAccount.lastName
      change.email = nextProps.userAccount.email
      change.contact = nextProps.userAccount.contact
      change.city = nextProps.userAccount.city
      change.activated = nextProps.userAccount.activated
      change.rolesIds = nextProps.userAccount.rolesIds
      change.positionId = nextProps.userAccount.positionId
      change.emailErrorText = ''

      this.setState({ users: change })
    }
    if (nextProps.shouldUpdate) {
      this.setState({
        shouldUpdate: this.props.shouldUpdate,
        users: {
          userId: '',
          firstName: '',
          lastName: '',
          email: '',
          contact: '',
          city: '',
          activated: true,
          rolesIds: [1],
          positionId: 1,
          editFirstName: '',
          editLastName: '',
          editEmail: '',
          editContact: '',
          editCity: '',
          emailErrorText: '' }
      })
    }
  }

  handleOpenEdit = () => {
    this.setState({ openEditInformation: true,
      users: { ...this.state.users,
        editFirstName: this.state.users.firstName,
        editLastName: this.state.users.lastName,
        editEmail: this.state.users.email,
        editContact: this.state.users.contact,
        editCity: this.state.users.city
      }
    })
  }

  handleCloseEdit = () => {
    this.setState({ openEditInformation: false,
      users: {
        editFirstName: '',
        editLastName: '',
        editEmail: '',
        editContact: '',
        editCity: ''
      }
    }, () => this.props.getUsers())
  }

  handleOpenPassword = () => {
    this.setState({ openPasswordInormation: true })
  }

  handleClosePassword = () => {
    this.setState({ openPasswordInormation: false })
  }

  handleChange (name, e) {
    let change = { ...this.state }
    change.users[name] = e.target.value
    this.setState(change)
  }

  checkEmail (email) {
    /*eslint-disable */
    let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    /*eslint-enable */
    let errorEmail = email + 'Error'
    let change = {}
    if (this.state.users.editEmail === '') {
      change[errorEmail] = 'The field is required'
    } else {
      change[errorEmail] = ''
    }
    this.setState(change)
  }

  editUsers = () => {
    let newDataUser = {
      firstName: this.state.users.editFirstName,
      lastName: this.state.users.editLastName,
      email: this.state.users.editEmail,
      contact: this.state.users.editContact,
      city: this.state.users.editCity,
      activated: true,
      rolesIds: [1],
      positionId: 1
    }
    this.props.editUsers(newDataUser)
    this.setState({
      openEditInformation: false,
      users: {
        editFirstName: '',
        editLastName: '',
        editEmail: '',
        editContact: '',
        editCity: ''
      } }, () => this.props.getUsers())
  }

  render () {
    const actionsEdit = [
      <FlatButton
        label='Save'
        secondary
        onClick={() => { this.editUsers() }}
        disabled={this.state.users.editFirstName.length > 50 ||
        this.state.users.editLastName.length > 50 ||
        this.state.users.editEmail.length > 50 ||
        this.state.users.editContact.length > 50 ||
        this.state.users.editCity.length > 50 ||
        this.state.users.editFirstName.length === 0 ||
        this.state.users.editLastName.length === 0 ||
        this.state.users.editEmail.length === 0
        }
      />,
      <FlatButton
        label='Cancel'
        secondary
        onClick={this.handleCloseEdit}
      />
    ]
    return (
      <div>
        {this.props.isLoggedIn
      ? <div className='container decision-support-centre-content limited-width'>
        <Dialog
          title='Edit information'
          actions={actionsEdit}
          modal={false}
          open={this.state.openEditInformation}
          onRequestClose={this.handleCloseEdit}
      >
          <div>
            <div className='text-title-edit'>First Name <i>(required)</i></div>
            <TextField
              className='first-name'
              fullWidth
              value={this.state.users.editFirstName}
              underlineStyle={{ width: 400 }}
              hintText='Enter first name'
              onChange={this.handleChange.bind(this, 'editFirstName')}
              errorText={this.state.users.editFirstName.length > 50 && 'Maximum 50 characters'}
          />
          </div>
          <div>
            <div className='text-title-edit'>Last Name <i>(required)</i></div>
            <TextField
              className='last-name'
              fullWidth
              hintText='Enter last name'
              value={this.state.users.editLastName}
              underlineStyle={{ width: 400 }}
              onChange={this.handleChange.bind(this, 'editLastName')}
              errorText={this.state.users.editLastName.length > 50 && 'Maximum 50 characters'}
          />
          </div>
          <div>
            <div className='text-title-edit'>E-mail <i>(required)</i></div>
            <TextField
              className='e-mail'
              fullWidth
              hintText='Enter email'
              value={this.state.users.editEmail}
              underlineStyle={{ width: 400 }}
              onChange={this.handleChange.bind(this, 'editEmail')}
              onBlur={() => { this.checkEmail('editEmail') }}
              errorText={this.state.users.editEmail.length > 50 && 'Maximum 50 characters'}
          />
          </div>
          <div>
            <div className='text-title-edit'>Contact information</div>
            <TextField
              className='contact'
              fullWidth
              hintText='Enter contact information'
              value={this.state.users.editContact}
              multiLine
              underlineStyle={{ width: 400 }}
              onChange={this.handleChange.bind(this, 'editContact')}
              errorText={this.state.users.editContact.length > 50 && 'Maximum 50 characters'}
          />
          </div>
          <div>
            <div className='text-title-edit'>City</div>
            <TextField
              className='city-management'
              fullWidth
              hintText='Enter city'
              value={this.state.users.editCity}
              underlineStyle={{ width: 400 }}
              onChange={this.handleChange.bind(this, 'editCity')}
              errorText={this.state.users.editCity.length > 50 && 'Maximum 50 characters'}
          />
          </div>
        </Dialog>
        <ResetDialog
          openReset={this.state.openPasswordInormation}
          closeResetDialog={this.handleClosePassword}
          userValue={this.state.users}
          handleSave={this.props.resetPassword}
          mode={'changeUserPassword'}
        />
        <div className='decision-support-centre-content'>
          <div className='support-section'>
            <div className='title'>
              <div className='text-underscore'>User Account<span className='dot'> .</span></div>
            </div>
            <div>
              <div className='description' style={{ marginBottom: 40 }}>
            Data: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
            ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis
            parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,
            pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec
            pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo,
            rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede
            mollis pretium. Integer tincidunt.
            </div>
              <div className='description' style={{ display: 'flow-root', marginBottom: 30 }}>
                <div style={{ display: 'flex', marginBottom: 10 }}>
                  <div style={{ marginRight: 10, textDecoration: 'underline' }}>First Name:</div>
                  <div>{this.state.users.firstName}</div></div>
                <div style={{ display: 'flex', marginBottom: 10 }}>
                  <div style={{ marginRight: 10, textDecoration: 'underline' }}>Last Name:</div>
                  <div>{this.state.users.lastName}</div></div>
                <div style={{ display: 'flex', marginBottom: 10 }}>
                  <div style={{ marginRight: 10, textDecoration: 'underline' }}>e-mail:</div>
                  <div>{this.state.users.email}</div></div>
                <div style={{ display: 'flex', marginBottom: 10 }}>
                  <div style={{ marginRight: 10, textDecoration: 'underline' }}>Contact information:</div>
                  <div>{this.state.users.contact}</div><div /></div>
                <div style={{ display: 'flex', marginBottom: 10 }}>
                  <div style={{ marginRight: 10, textDecoration: 'underline' }}>City:</div>
                  <div>{this.state.users.city}</div><div /></div>
              </div>
            </div>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <div style={{ marginRight: 5 }}>
                <RaisedButton
                  label='Edit information'
                  secondary
                  onClick={() => this.handleOpenEdit()} />
              </div>
              <div>
                <RaisedButton
                  label='Change password'
                  secondary
                  onClick={this.handleOpenPassword} />
              </div>
            </div>
          </div>
        </div>
      </div>
      : <div className='container decision-support-centre-content limited-width'>
        <div className='title'>
          <div className='text-underscore'>User Account<span className='dot'> .</span></div>
        </div>
        <div className='access-section' style={{ display: 'flex', flexDirection: 'column' }}>
          <div className='description'>
            If you want to access User Account you have to be logged in.
            <br /><Divider style={styles.divider} />
          </div>
          <div className='button-dsc' style={{ width: '100%', textAlign: 'center', marginTop: 10 }}>
            <RaisedButton
              label='Login'
              secondary
              onClick={() => browserHistory.push('/login')} />
          </div>
        </div>
      </div>
      }
      </div>
    )
  }
}

export default UserAccountContent
